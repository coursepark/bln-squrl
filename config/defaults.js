'use strict';

const secretKeyPrefix = 'SECRET_';

module.exports = {
	yamlStyle: {lineWidth: -1},
	acceptedEncodingSet: ['utf8', 'base64', 'hex'],
	encoding: 'utf8',
	secretKeyPrefix,
	hashSecretKeyName: 'HASH_SECRET_KEY',
	// eslint-disable-next-line security/detect-non-literal-regexp
	secretKeyRegex: new RegExp(`^[^:]{1,100}:${secretKeyPrefix}[A-Za-z0-9+/=]{44}$`, 'u'), // /^[^:]{1,100}:SECRET_[A-Za-z0-9+/=]{44}$/u,
	vault: 'vault',
};
