/* eslint-disable promise/prefer-await-to-callbacks, no-sync */
/* eslint-disable no-unused-vars */

'use strict';

const OnePasswordSecretStore = require('./index');
const OnePasswordSessionProvider = require('./session');
const execAsync = require('../../utils/exec_async');
const sinon = require('sinon');
const UnitTestRun = require('../../utils/unit_test_run');

describe('OnePasswordSecretStore', function () {
	it('should be a class', function () {
		const opss = new OnePasswordSecretStore();
		expect(opss).to.be.a.instanceof(OnePasswordSecretStore);
	});

	it('should provide "get" method', function () {
		const opss = new OnePasswordSecretStore();
		expect(opss)
			.to.have.property('get')
			.that.is.a('function');
	});

	describe('get method', function () {
		let test;
		let opsp;

		beforeEach(function () {
			test = new UnitTestRun('get', OnePasswordSecretStore);
			sinon.stub(execAsync, 'execAsync');
			opsp = sinon.createStubInstance(OnePasswordSessionProvider);
		});

		afterEach(function () {
			execAsync.execAsync.restore();
		});

		describe('passed valid key', function () {
			it('should return value', async function () {
				opsp.get.returns(Promise.resolve({session: '<session>', account: '<account>'}));
				execAsync.execAsync.returns(
					Promise.resolve(JSON.stringify({details: {notesPlain: Buffer.from('<value>').toString('base64')}}))
				);
				await test.construct({sessionProvider: opsp}).run('<key>');
				expect(test.returns.toString()).to.be.equal('<value>');
				expect(opsp.get.calledOnce).to.be.true;
			});
		});

		describe('passed invalid key', function () {
			it('should return undefined', async function () {
				opsp.get.returns(Promise.resolve({session: '<session>', account: '<account>'}));
				execAsync.execAsync.throws(new Error('<pre> not found <post>'));
				await test.construct({sessionProvider: opsp}).run('<key>');
				expect(test.returns).to.be.undefined;
				expect(opsp.get.calledOnce).to.be.true;
			});
		});

		describe('passed key of "kube HASH_SECRET" which is not base64 encoded on ', function () {
			it('should return value', async function () {
				opsp.get.returns(Promise.resolve({session: '<session>', account: '<account>'}));
				execAsync.execAsync.returns(Promise.resolve(JSON.stringify({details: {notesPlain: '<value>'}})));
				await test
					.construct({sessionProvider: opsp, hashSecretKeyName: '<hashSecretKeyName>'})
					.run('<hashSecretKeyName>');
				expect(test.returns.toString()).to.be.equal('<value>');
				expect(opsp.get.calledOnce).to.be.true;
			});
		});

		describe('unable get a session', function () {
			it('should throw an error', async function () {
				opsp.get.returns(Promise.resolve({}));
				await test.construct({sessionProvider: opsp}).run('<key>');
				expect(test.error.message).to.equal('no session');
				expect(opsp.get.calledOnce).to.be.true;
				expect(execAsync.execAsync.called).to.be.false;
			});
		});

		describe('1Password give an error while trying to create a secure note', function () {
			it('should throw error', async function () {
				opsp.get.returns(Promise.resolve({session: '<session>', account: '<account>'}));
				execAsync.execAsync.returns(Promise.reject(new Error('<error_from_1Password>')));
				await test.construct({sessionProvider: opsp}).run('<key>');
				expect(test.error.message).to.equal('error from 1Password - <error_from_1Password>');
				expect(opsp.get.calledOnce).to.be.true;
				expect(execAsync.execAsync.calledOnce).to.be.true;
			});
		});

		describe('session has expired just before 1Password create secure note, must get a new session', function () {
			it('should try getting session a second time', async function () {
				opsp.get
					.onFirstCall()
					.returns(Promise.resolve({session: '<expired_session>', account: '<account>'})) // eslint-disable-line newline-per-chained-call
					.onSecondCall()
					.returns(Promise.resolve({session: '<session>', account: '<account>'})); // eslint-disable-line newline-per-chained-call
				execAsync.execAsync
					.onFirstCall()
					.returns(Promise.reject(new Error('<pre> Authentication required <post>'))) // eslint-disable-line newline-per-chained-call
					.onSecondCall()
					.returns(
						Promise.resolve(
							// eslint-disable-line newline-per-chained-call
							JSON.stringify({details: {notesPlain: Buffer.from('<value>').toString('base64')}})
						)
					);

				await test.construct({sessionProvider: opsp}).run('<key>');

				expect(test.returns.toString()).to.be.equal('<value>');
				expect(opsp.get.calledTwice).to.be.true;
				expect(execAsync.execAsync.calledTwice).to.be.true;
			});
		});
	});

	it('should provide "set" method', function () {
		const opss = new OnePasswordSecretStore();
		expect(opss)
			.to.have.property('set')
			.that.is.a('function');
	});

	describe('set method', function () {
		let test;
		let opsp;

		beforeEach(function () {
			test = new UnitTestRun('set', OnePasswordSecretStore);
			sinon.stub(execAsync, 'execAsync');
			opsp = sinon.createStubInstance(OnePasswordSessionProvider);
		});

		afterEach(function () {
			execAsync.execAsync.restore();
		});

		describe('passed a new key', function () {
			it('should set the value in 1Password', async function () {
				opsp.get.returns(Promise.resolve({session: '<session>', account: '<account>'}));
				execAsync.execAsync.onFirstCall().throws(new Error('<pre> not found <post>'));

				await test.construct({sessionProvider: opsp}).run('<key>', Buffer.from('<value>'));

				expect(opsp.get.calledTwice).to.be.true;
				expect(execAsync.execAsync.secondCall.args[0]).to.include('<session>');
				expect(execAsync.execAsync.secondCall.args[0]).to.include('<key>');
				expect(execAsync.execAsync.secondCall.args[0]).to.include(Buffer.from('<value>').toString('base64'));
			});
		});

		describe('passed new value for an existing key', function () {
			it('should set the value in 1Password', async function () {
				opsp.get.returns(Promise.resolve({session: '<session>', account: '<account>'}));
				execAsync.execAsync
					.onFirstCall()
					.returns(
						Promise.resolve(
							JSON.stringify({
								details: {notesPlain: Buffer.from('<value>').toString('base64')},
							})
						)
					)
					.onSecondCall()
					.returns(Promise.resolve());

				await test.construct({sessionProvider: opsp}).run('<key>', Buffer.from('<new_value>'));

				expect(opsp.get.calledTwice).to.be.true;
				expect(execAsync.execAsync.secondCall.args[0]).to.include('<session>');
				expect(execAsync.execAsync.secondCall.args[0]).to.include('<key>');
				// eslint-disable-next-line max-len
				expect(execAsync.execAsync.secondCall.args[0]).to.include(Buffer.from('<new_value>').toString('base64'));
			});
		});

		describe('passed the same value for an existing key', function () {
			it('should set the value in 1Password', async function () {
				opsp.get.returns(Promise.resolve({session: '<session>', account: '<account>'}));
				execAsync.execAsync.returns(
					Promise.resolve(JSON.stringify({details: {notesPlain: Buffer.from('<value>').toString('base64')}}))
				);

				await test.construct({sessionProvider: opsp}).run('<key>', Buffer.from('<value>'));

				expect(opsp.get.called).to.be.true;
				expect(execAsync.execAsync.calledOnce).to.be.true;
			});
		});

		describe('unable get a session', function () {
			it('should throw error', async function () {
				opsp.get
					.onFirstCall()
					.returns(Promise.resolve({session: '<session>', account: '<account>'}))
					.onSecondCall()
					.returns(Promise.resolve({}));
				execAsync.execAsync.onFirstCall().throws(new Error('<pre> not found <post>'));

				await test.construct({sessionProvider: opsp}).run('<key>', Buffer.from('<value>'));

				expect(opsp.get.calledTwice).to.be.true;
				expect(execAsync.execAsync.calledOnce).to.be.true;
				expect(test.error.message).to.equal('no session');
			});
		});

		describe('1Password gives an error while trying to create a secure note', function () {
			it('should throw error', async function () {
				opsp.get.returns(Promise.resolve({session: '<session>', account: '<account>'}));
				execAsync.execAsync
					.onFirstCall()
					.throws(new Error('<pre> not found <post>'))
					.onSecondCall()
					.returns(Promise.reject(new Error('<error_from_1Password>')));

				await test.construct({sessionProvider: opsp}).run('<key>', Buffer.from('<value>'));

				expect(test.error.message).to.equal('error from 1Password - <error_from_1Password>');
				expect(opsp.get.calledTwice).to.be.true;
				expect(execAsync.execAsync.calledTwice).to.be.true;
			});
		});

		describe('session has expired just before 1Password create secure note, must get a new session', function () {
			it('should try getting session a second time', async function () {
				opsp.get
					.onFirstCall()
					.returns(Promise.resolve({session: '<session_session>', account: '<account>'}))
					.onSecondCall()
					.returns(Promise.resolve({session: '<expired_session>', account: '<account>'}))
					.onThirdCall()
					.returns(Promise.resolve({session: '<session>', account: '<account>'})); // eslint-disable-line newline-per-chained-call
				execAsync.execAsync
					.onFirstCall()
					.throws(new Error('<pre> not found <post>'))
					.onSecondCall()
					.returns(Promise.reject(new Error('<pre> Authentication required <post>')))
					.onThirdCall()
					.returns(
						Promise.resolve(
							// eslint-disable-line newline-per-chained-call
							JSON.stringify({details: {notesPlain: Buffer.from('<value>').toString('base64')}})
						)
					);

				await test.construct({sessionProvider: opsp}).run('<key>', Buffer.from('<value>'));

				expect(opsp.get.calledThrice).to.be.true;
				expect(execAsync.execAsync.calledThrice).to.be.true;
			});
		});
	});
});
