'use strict';

const execAsync = require('../../utils/exec_async');
const OnePasswordSessionProvider = require('./session');

module.exports = class OnePasswordSecretStore {
	constructor(options = {}) {
		this.hashSecretKeyName = options.hashSecretKeyName;
		this.sessionProvider
			= options.sessionProvider
			|| new OnePasswordSessionProvider({
				shorthand: options.shorthand,
				account: options.account,
				email: options.email,
				secretKey: options.secretKey,
				password: options.password,
				session: options.session,
			});
	}

	async get(key, vault) {
		let invalidateSession = false;
		// eslint-disable-next-line no-constant-condition
		while (true) {
			// eslint-disable-next-line no-await-in-loop
			const {session, shorthand} = await this.sessionProvider.get(invalidateSession);
			if (!session) {
				throw new Error('no session');
			}
			try {
				// eslint-disable-next-line no-await-in-loop
				const result = await execAsync.execAsync(
					`OP_SESSION_${shorthand}=${session} op get item --vault='${vault}' '${key}'`
				);
				if (key && key === this.hashSecretKeyName) {
					// the hash secret key is not base64 encoded as it is manually maintained
					return JSON.parse(result).details.notesPlain;
				}
				return Buffer.from(JSON.parse(result).details.notesPlain, 'base64');
			}
			catch (err) {
				if (err.message.includes('not found')) {
					return undefined;
				}
				else if (
					false
					|| err.message.includes('Authentication required')
					|| err.message.includes('not currently signed in')
				) {
					invalidateSession = true;
				}
				else {
					throw new Error(`error from 1Password - ${err.message}`);
				}
			}
		}
	}

	async set(key, value, vault) {
		const getValue = await this.get(key, vault);
		if (getValue && Buffer.compare(getValue, value) === 0) {
			// already set
			return;
		}
		let invalidateSession = false;
		// eslint-disable-next-line no-constant-condition
		while (true) {
			// eslint-disable-next-line no-await-in-loop
			const {session, shorthand} = await this.sessionProvider.get(invalidateSession);
			if (!session) {
				throw new Error('no session');
			}
			try {
				const itemStr = JSON.stringify({notesPlain: Buffer.from(value).toString('base64'), sections: []});
				// eslint-disable-next-line no-await-in-loop
				await execAsync.execAsync(
					// eslint-disable-next-line max-len
					`OP_SESSION_${shorthand}=${session} op create item 'secure note' --title='${key}' --vault='${vault}' '${itemStr}'`
				);
				return;
			}
			catch (err) {
				if (
					false
					|| err.message.includes('Authentication required')
					|| err.message.includes('not currently signed in')
				) {
					invalidateSession = true;
				}
				else {
					throw new Error(`error from 1Password - ${err.message}`);
				}
			}
		}
	}
};
