'use strict';

const execAsync = require('../../../utils/exec_async');
const fs = require('fs');
const readline = require('readline');
const stream = require('stream');
const yaml = require('js-yaml'); // convert to and from js objects to yaml

const credentialsFile = './secret-cred.yaml';
const yamlStyle = {lineWidth: -1};

module.exports = class OnePasswordSessionProvider {
	constructor(options = {}) {
		this.useStore
			= options.useStore === undefined
				? !options.account && !options.email && !options.secretKey && !options.session
				: Boolean(options.useStore);
		this.shorthand = options.shorthand;
		this.account = options.account;
		this.email = options.email;
		this.secretKey = options.secretKey;
		this.password = options.password;
		this.session = options.session;
	}

	static async getCliQuestion(question, showResponse = true) {
		const switchStdout = new stream.Writable({
			// used to control output during typing when using readline for password entry
			// eslint-disable-next-line promise/prefer-await-to-callbacks
			write(chunk, encoding, callback) {
				if (this.on) {
					process.stdout.write(chunk, encoding);
				}
				callback(); // eslint-disable-line promise/prefer-await-to-callbacks
			},
		});

		return new Promise((resolve) => {
			const rl = readline.createInterface({
				input: process.stdin,
				output: switchStdout,
				terminal: true,
			});
			switchStdout.on = true;
			rl.question(`${question}: `, (v) => {
				if (!showResponse) {
					process.stdout.write('\n');
				}
				rl.close();
				resolve(v);
			});
			switchStdout.on = showResponse;
		});
	}

	static async getCliSession(shorthand, account, email, secretKey, password) {
		let session;
		const Opsp = OnePasswordSessionProvider;

		const _shorthand = shorthand || await Opsp.getCliQuestion('1Password shorthand');
		const _account = account || await Opsp.getCliQuestion('1Password account (like "<account>.1password.com")');
		const _email = email || await Opsp.getCliQuestion("1Password user's email");
		const _secretKey = secretKey || await Opsp.getCliQuestion("1Password user's secret-key (not password)");
		const _password = password || await Opsp.getCliQuestion('1Password master password', false);

		if (_password.trim() === '') {
			throw new Error('must provide a password');
		}

		try {
			const result = await execAsync.execAsync(
				`echo '${_password}' | op signin `
					+ `--shorthand='${_shorthand}' `
					+ `'${_account}.1password.com' '${_email}' ${_secretKey}`
			);
			if (!result.includes('OP_SESSION_')) {
				throw new Error('1Password cli signin not behaving as expected');
			}
			session = result.split('\n')[0].replace(/.*OP_SESSION_[^=]*="(.*)".*$/u, '$1');
		}
		catch (err) {
			if (err.message.includes('Authentication required')) {
				throw new Error('password not accepted by 1Password');
			}
			else {
				const msg = err.message.replace(_password, '<password>').replace(_secretKey, '<secretKey>');
				throw new Error(`error from 1Password - ${msg}`);
			}
		}

		return {shorthand: _shorthand, account: _account, email: _email, secretKey: _secretKey, session};
	}

	static async readCredentialFile() {
		try {
			// eslint-disable-next-line no-sync, security/detect-non-literal-fs-filename
			return yaml.safeLoad(fs.readFileSync(credentialsFile, 'utf8'));
		}
		catch (err) {
			return {};
		}
	}

	static async writeCredentialFile(contentObj) {
		const fileContent = yaml.safeDump(contentObj, yamlStyle);
		try {
			// eslint-disable-next-line no-sync, security/detect-non-literal-fs-filename
			fs.writeFileSync(credentialsFile, fileContent, 'utf8');
		}
		catch (err) {
			// don't care if can't write session to file
		}
	}

	static async getStoredCredentials(shorthand, account, email, secretKey, session) {
		const fileContent = await OnePasswordSessionProvider.readCredentialFile();
		let content = fileContent['1Password'];
		// ensure content is plain object, otherwise use empty object
		content = Object(content) === content ? content : {};
		return {
			shorthand: content.shorthand || shorthand,
			account: content.account || account,
			email: content.email || email,
			secretKey: content.secretKey || secretKey,
			session: content.session || session,
		};
	}

	static async setStoredCredentials(shorthand, account, email, secretKey, session) {
		const contentObj = await OnePasswordSessionProvider.readCredentialFile();
		if (!contentObj['1Password']) {
			contentObj['1Password'] = {};
		}
		const opCrendentials = contentObj['1Password'];
		if (
			true
			&& shorthand === opCrendentials.shorthand
			&& account === opCrendentials.account
			&& email === opCrendentials.email
			&& secretKey === opCrendentials.secretKey
			&& session === opCrendentials.session
		) {
			// same, don't continue file change
			return;
		}
		// only applies non undefined properties
		Object.assign(opCrendentials, JSON.parse(JSON.stringify({shorthand, account, email, secretKey, session})));
		await OnePasswordSessionProvider.writeCredentialFile(contentObj);
	}

	async get(invalidateSession = false) {
		const Opsp = OnePasswordSessionProvider;
		let {shorthand, account, email, secretKey, password, session} = this;
		if (invalidateSession) {
			session = null;
			if (this.useStore) {
				await Opsp.setStoredCredentials(shorthand, account, email, secretKey, session);
			}
		}

		if ((!session || !shorthand) && this.useStore) {
			({shorthand, account, email, secretKey, session} = await Opsp.getStoredCredentials(
				shorthand,
				account,
				email,
				secretKey,
				session
			));
		}

		if (!session || !shorthand) {
			({shorthand, account, email, secretKey, password, session} = await Opsp.getCliSession(
				shorthand,
				account,
				email,
				secretKey,
				password
			));
		}

		if (this.useStore) {
			await Opsp.setStoredCredentials(shorthand, account, email, secretKey, session);
		}
		this.session = session;
		this.shorthand = shorthand;
		return {session, shorthand};
	}
};
