/* eslint-disable promise/prefer-await-to-callbacks, no-sync */
/* eslint-disable no-unused-vars */

'use strict';

const OnePasswordSessionProvider = require('./index');
const execAsync = require('../../../utils/exec_async');
const fs = require('fs');
const readline = require('readline');
const sinon = require('sinon');
const UnitTestRun = require('../../../utils/unit_test_run');

describe('OnePasswordSessionProvider', function () {
	it('should be a class', function () {
		const opsp = new OnePasswordSessionProvider();
		expect(opsp).to.be.a.instanceof(OnePasswordSessionProvider);
	});

	it('should provide "get" method', function () {
		const opsp = new OnePasswordSessionProvider();
		expect(opsp)
			.to.have.property('get')
			.that.is.a('function');
	});

	it('should have static method "getCliQuestion"', function () {
		expect(OnePasswordSessionProvider)
			.to.have.property('getCliQuestion')
			.that.is.a('function');
	});

	describe('static method "getCliQuestion"', function () {
		let test;

		beforeEach(function () {
			test = new UnitTestRun(OnePasswordSessionProvider.getCliQuestion);
			const rl = {};
			sinon.stub(readline, 'createInterface').callsFake(() => {
				// eslint-disable-next-line promise/prefer-await-to-callbacks
				rl.question = function (output, callback) {
					test.stdout = output;
					callback(test.stdin);
				};
				rl.close = function () {};
				return rl;
			});
		});

		afterEach(function () {
			readline.createInterface.restore();
		});

		it('should put to message to stdout (via readline lib)', async function () {
			await test.run('<question>');
			expect(test.stdout).to.equal('<question>: ');
		});

		it('should provide response, entered at stdin', async function () {
			test.stdin = '<response>';
			await test.run('<question>');
			expect(test.returns).to.equal(test.stdin);
		});
	});

	it('should have static method "getCliSession"', function () {
		expect(OnePasswordSessionProvider)
			.to.have.property('getCliSession')
			.that.is.a('function');
	});

	describe('static method "getCliSession"', function () {
		let test;

		beforeEach(function () {
			test = new UnitTestRun(OnePasswordSessionProvider.getCliSession);
			sinon.stub(OnePasswordSessionProvider, 'getCliQuestion');
			sinon.stub(execAsync, 'execAsync');
		});

		afterEach(function () {
			OnePasswordSessionProvider.getCliQuestion.restore();
			execAsync.execAsync.restore();
		});

		it('should use getCliQuestion for password if password not provided', async function () {
			OnePasswordSessionProvider.getCliQuestion.returns(Promise.resolve('<password>'));
			execAsync.execAsync.returns(Promise.resolve('<pre> OP_SESSION_<shorthand>="<session>" <post>'));
			await test.run('<shorthand>', '<account>', '<email>', '<secretKey>', null);
			expect(OnePasswordSessionProvider.getCliQuestion.calledOnce).to.be.true;
			expect(execAsync.execAsync.calledOnce).to.be.true;
		});

		it('should not use getCliQuestion if password is provided', async function () {
			OnePasswordSessionProvider.getCliQuestion.returns(Promise.resolve('<password>'));
			execAsync.execAsync.returns(Promise.resolve('<pre> OP_SESSION_<shorthand>="<session>" <post>'));
			await test.run('<shorthand>', '<account>', '<email>', '<secretKey>', '<password>');
			expect(OnePasswordSessionProvider.getCliQuestion.calledOnce).to.be.false;
			expect(execAsync.execAsync.calledOnce).to.be.true;
		});

		it('should use 1Password cli internally with params provided', async function () {
			execAsync.execAsync.returns(Promise.resolve('<pre> OP_SESSION_<shorthand>="<session>" <post>'));
			await test.run('<shorthand>', '<account>', '<email>', '<secretKey>', '<provided_password>');
			expect(test.returns).to.deep.equal({
				shorthand: '<shorthand>',
				account: '<account>',
				email: '<email>',
				secretKey: '<secretKey>',
				session: '<session>',
			});
			expect(OnePasswordSessionProvider.getCliQuestion.calledOnce).to.be.false;
			expect(execAsync.execAsync.calledOnce).to.be.true;
		});

		it('should use 1Password cli internally with password from getCliQuestion', async function () {
			OnePasswordSessionProvider.getCliQuestion.returns(Promise.resolve('<cli_password>'));
			execAsync.execAsync.returns(Promise.resolve('<pre> OP_SESSION_<shorthand>="<session>" <post>'));
			await test.run('<shorthand>', '<account>', '<email>', '<secretKey>');
			expect(test.returns).to.deep.equal({
				shorthand: '<shorthand>',
				account: '<account>',
				email: '<email>',
				secretKey: '<secretKey>',
				session: '<session>',
			});
			expect(execAsync.execAsync.calledOnce).to.be.true;
			expect(execAsync.execAsync.calledOnce).to.be.true;
		});

		it('should throw error if password left blank', async function () {
			OnePasswordSessionProvider.getCliQuestion.returns(Promise.resolve(''));
			await test.run('<shorthand>', '<account>', '<email>', '<secretKey>', null);
			expect(test.error.message).to.be.equal('must provide a password');
			expect(OnePasswordSessionProvider.getCliQuestion.calledOnce).to.be.true;
			expect(execAsync.execAsync.calledOnce).to.be.false;
		});

		it('should throw error if password not accepted by 1Password', async function () {
			OnePasswordSessionProvider.getCliQuestion.returns(Promise.resolve('<invalid_password>'));
			execAsync.execAsync.returns(Promise.reject(new Error('<pre> Authentication required <post>')));
			await test.run('<shorthand>', '<account>', '<email>', '<secretKey>', null);
			expect(test.error.message).to.be.equal('password not accepted by 1Password');
			expect(OnePasswordSessionProvider.getCliQuestion.calledOnce).to.be.true;
			expect(execAsync.execAsync.calledOnce).to.be.true;
		});

		it('should throw error if 1Password errors unexpectedly', async function () {
			OnePasswordSessionProvider.getCliQuestion.returns(Promise.resolve('<password>'));
			execAsync.execAsync.returns(Promise.reject(new Error('<unknown error>')));
			await test.run('<shorthand>', '<account>', '<email>', '<secretKey>', null);
			expect(test.error.message).to.be.equal('error from 1Password - <unknown error>');
			expect(OnePasswordSessionProvider.getCliQuestion.calledOnce).to.be.true;
			expect(execAsync.execAsync.calledOnce).to.be.true;
		});

		it('should throw error if 1Password returns incompatible output', async function () {
			OnePasswordSessionProvider.getCliQuestion.returns(Promise.resolve('<password>'));
			execAsync.execAsync.returns(Promise.resolve('<pre>\nincompatible output\n<post>'));
			await test.run('<shorthand>', '<account>', '<email>', '<secretKey>', null);
			// eslint-disable-next-line max-len
			expect(test.error.message).to.be.equal('error from 1Password - 1Password cli signin not behaving as expected');
			expect(OnePasswordSessionProvider.getCliQuestion.calledOnce).to.be.true;
			expect(execAsync.execAsync.calledOnce).to.be.true;
		});
	});

	describe('static method "readCredentialFile"', function () {
		let test;

		beforeEach(function () {
			test = new UnitTestRun(OnePasswordSessionProvider.readCredentialFile);
			sinon.stub(fs, 'readFileSync');
		});

		afterEach(function () {
			fs.readFileSync.restore();
		});

		describe('credentials file exists and has valid content', function () {
			it('should read the credentials file and parse yaml', async function () {
				fs.readFileSync.returns(
					''
						+ '1Password:\n'
						+ '  account: <account>\n'
						+ '  email: <email>\n'
						+ '  secretKey: <secretKey>\n'
						+ '  session: <session>\n'
				);
				await test.run();
				expect(test.returns).to.deep.equal({
					'1Password': {
						account: '<account>',
						email: '<email>',
						secretKey: '<secretKey>',
						session: '<session>',
					},
				});
			});
		});

		describe('credentials file reading throws an error', function () {
			it('should return an empty object', async function () {
				fs.readFileSync.throws(new Error());
				await test.run();
				expect(test.returns).to.deep.equal({});
			});
		});
	});

	describe('static method "writeCredentialFile"', function () {
		describe('writes file successfully with valid content', function () {
			it('should stringify yaml and write the credentials file', async function () {
				const writeFileSync = sinon.stub(fs, 'writeFileSync');
				await OnePasswordSessionProvider.writeCredentialFile({
					'1Password': {
						account: '<account>',
						email: '<email>',
						secretKey: '<secretKey>',
						session: '<session>',
					},
				});
				fs.writeFileSync.restore();
				expect(writeFileSync.calledOnce).to.be.true;
				expect(writeFileSync.getCall(0).args[0]).to.deep.equal('./secret-cred.yaml');
				expect(writeFileSync.getCall(0).args[1]).to.deep.equal(
					''
						+ '1Password:\n'
						+ '  account: <account>\n'
						+ '  email: <email>\n'
						+ '  secretKey: <secretKey>\n'
						+ '  session: <session>\n'
				);
			});
		});

		describe('credentials file writing throws an error', function () {
			it('should just continue', async function () {
				sinon.stub(fs, 'writeFileSync').throws(new Error());
				const response = await OnePasswordSessionProvider.writeCredentialFile({
					'1Password': {
						account: '<account>',
						email: '<email>',
						secretKey: '<secretKey>',
						session: '<session>',
					},
				});
				fs.writeFileSync.restore();
				expect(response).to.equal(undefined);
			});
		});
	});

	describe('static method "getStoredCredentials"', function () {
		let test;

		beforeEach(function () {
			test = new UnitTestRun(OnePasswordSessionProvider.getStoredCredentials);
			sinon.stub(OnePasswordSessionProvider, 'readCredentialFile');
		});

		afterEach(function () {
			OnePasswordSessionProvider.readCredentialFile.restore();
		});

		describe('credentials file has invalid content', function () {
			it('should return undefined credentials', async function () {
				OnePasswordSessionProvider.readCredentialFile.returns({Not1Password: {}});
				await test.run();
				expect(test.returns).to.deep.equal({
					shorthand: undefined,
					account: undefined,
					email: undefined,
					secretKey: undefined,
					session: undefined,
				});
			});
		});

		describe('credentials file is empty', function () {
			it('should return undefined credentials', async function () {
				OnePasswordSessionProvider.readCredentialFile.returns({});
				const response = await OnePasswordSessionProvider.getStoredCredentials();
				expect(response).to.deep.equal({
					shorthand: undefined,
					account: undefined,
					email: undefined,
					secretKey: undefined,
					session: undefined,
				});
			});
		});

		describe('credentials file has string instead of object for 1Password', function () {
			it('should return undefined credentials', async function () {
				OnePasswordSessionProvider.readCredentialFile.returns({'1Password': 'Hello World'});
				const response = await OnePasswordSessionProvider.getStoredCredentials();
				expect(response).to.deep.equal({
					shorthand: undefined,
					account: undefined,
					email: undefined,
					secretKey: undefined,
					session: undefined,
				});
			});
		});

		describe('credentials file has null instead of object for 1Password', function () {
			it('should return undefined credentials', async function () {
				OnePasswordSessionProvider.readCredentialFile.returns({'1Password': null});
				const response = await OnePasswordSessionProvider.getStoredCredentials();
				expect(response).to.deep.equal({
					shorthand: undefined,
					account: undefined,
					email: undefined,
					secretKey: undefined,
					session: undefined,
				});
			});
		});
	});

	describe('static method "setStoredCredentials"', function () {
		let test;

		beforeEach(function () {
			test = new UnitTestRun(OnePasswordSessionProvider.setStoredCredentials);
			sinon.stub(OnePasswordSessionProvider, 'readCredentialFile');
			sinon.stub(OnePasswordSessionProvider, 'writeCredentialFile');
		});

		afterEach(function () {
			OnePasswordSessionProvider.readCredentialFile.restore();
			OnePasswordSessionProvider.writeCredentialFile.restore();
		});

		describe('set only account, email, secretKey, and session, credentials file has content', function () {
			it('should update session but not change anything else', async function () {
				OnePasswordSessionProvider.readCredentialFile.returns({
					'1Password': {
						shorthand: '<shorthand>',
						account: '<account>',
						email: '<email>',
						secretKey: '<secretKey>',
						session: '<session>',
					},
					other: 'stuff',
				});

				await test.run('<new_shorthand>', '<new_account>', '<new_email>', '<new_secretKey>', '<new_session>');

				expect(OnePasswordSessionProvider.writeCredentialFile.getCall(0).args[0]).to.deep.equal({
					'1Password': {
						shorthand: '<new_shorthand>',
						account: '<new_account>',
						email: '<new_email>',
						secretKey: '<new_secretKey>',
						session: '<new_session>',
					},
					other: 'stuff',
				});
			});
		});

		describe('credentials file is empty', function () {
			it('should create credentials properties', async function () {
				OnePasswordSessionProvider.readCredentialFile.returns({});

				await test.run('<new_shorthand>', '<new_account>', '<new_email>', '<new_secretKey>', '<new_session>');

				expect(OnePasswordSessionProvider.writeCredentialFile.getCall(0).args[0]).to.deep.equal({
					'1Password': {
						shorthand: '<new_shorthand>',
						account: '<new_account>',
						email: '<new_email>',
						secretKey: '<new_secretKey>',
						session: '<new_session>',
					},
				});
			});
		});

		describe("credentials don't not change from values in file", function () {
			it('should not write file', async function () {
				OnePasswordSessionProvider.readCredentialFile.returns({
					'1Password': {
						session: '<session>',
					},
				});

				await test.run(undefined, undefined, undefined, undefined, '<session>');

				expect(OnePasswordSessionProvider.writeCredentialFile.called).to.be.false;
			});
		});
	});

	describe('method "get"', function () {
		let test;

		beforeEach(function () {
			test = new UnitTestRun('get', OnePasswordSessionProvider);
			sinon.stub(OnePasswordSessionProvider, 'getStoredCredentials');
			sinon.stub(OnePasswordSessionProvider, 'setStoredCredentials');
			sinon.stub(OnePasswordSessionProvider, 'getCliSession');
		});

		afterEach(function () {
			OnePasswordSessionProvider.getStoredCredentials.restore();
			OnePasswordSessionProvider.setStoredCredentials.restore();
			OnePasswordSessionProvider.getCliSession.restore();
		});

		describe('with session and account from constructor param', function () {
			it('should return session and account without change', async function () {
				await test.construct({session: '<session>', shorthand: '<shorthand>'}).run();

				expect(OnePasswordSessionProvider.setStoredCredentials.calledOnce).to.be.false;
				expect(OnePasswordSessionProvider.getStoredCredentials.called).to.be.false;
				expect(OnePasswordSessionProvider.getCliSession.called).to.be.false;
				expect(test.returns.session).to.equal('<session>');
				expect(test.returns.shorthand).to.equal('<shorthand>');
			});
		});

		describe('with session from constructor param and force useStore', function () {
			it('should return session without change', async function () {
				OnePasswordSessionProvider.getStoredCredentials.returns({
					shorthand: undefined,
					account: undefined,
					email: undefined,
					secretKey: undefined,
					password: undefined,
					session: undefined,
				});

				await test.construct({session: '<session>', shorthand: '<shorthand>', useStore: true}).run();

				expect(OnePasswordSessionProvider.setStoredCredentials.calledOnce).to.be.true;
				expect(OnePasswordSessionProvider.getStoredCredentials.called).to.be.false;
				expect(OnePasswordSessionProvider.getCliSession.called).to.be.false;
				expect(test.returns.session).to.equal('<session>');
				expect(test.returns.shorthand).to.equal('<shorthand>');
			});
		});

		describe('with session from constructor param, invalidate passed to .get, new session from cli', function () {
			it('should return session without change', async function () {
				OnePasswordSessionProvider.getStoredCredentials.returns({
					shorthand: '<shorthand>',
					account: '<account>',
					email: '<email>',
					secretKey: '<secretKey>',
					session: undefined,
				});
				OnePasswordSessionProvider.getCliSession.returns({
					shorthand: '<shorthand>',
					account: '<account>',
					email: '<email>',
					secretKey: '<secretKey>',
					session: '<new_session>',
				});

				await test.construct({session: '<session>', shorthand: '<shorthand>', useStore: true}).run(true);

				expect(OnePasswordSessionProvider.setStoredCredentials.calledTwice).to.be.true;
				expect(OnePasswordSessionProvider.getStoredCredentials.called).to.be.true;
				expect(OnePasswordSessionProvider.getCliSession.called).to.be.true;
				expect(test.returns.session).to.equal('<new_session>');
				expect(test.returns.shorthand).to.equal('<shorthand>');
			});
		});

		describe('with session from credential file', function () {
			it('should get credentials', async function () {
				OnePasswordSessionProvider.getStoredCredentials.returns({
					shorthand: '<shorthand>',
					account: '<account>',
					email: '<email>',
					secretKey: '<secretKey>',
					password: '<password>',
					session: '<session>',
				});

				await test.construct({session: '<session>', shorthand: '<shorthand>', useStore: true}).run(true);

				expect(test.returns.session).to.equal('<session>');
				expect(test.returns.shorthand).to.equal('<shorthand>');
			});
		});
	});
});
