'use strict';

const aws = require('./aws');
const op = require('./op');

module.exports = {
	op,
	aws,
};
