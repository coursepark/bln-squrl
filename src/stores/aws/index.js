'use strict';

const AWS = require('aws-sdk');
class AWSSecretsManagerStore {
	constructor(options = {}) {
		this.hashSecretKeyName = options.hashSecretKeyName;
		const region = options.region || 'ca-central-1';
		AWS.config.update({region});
		this.secretsmanager = new AWS.SecretsManager();
	}

	async get(key, vault) {
		try {
			const data = await this.secretsmanager.getSecretValue({SecretId: `${vault}/${key}`}).promise();
			const secret = data.SecretString;
			if (key === this.hashSecretKeyName) {
				return secret;
			}
			return Buffer.from(secret, 'base64');
		}
		catch (err) {
			if (err.message.includes('can’t find')) {
				return undefined;
			}
			throw new Error(`error from AWS SDK - ${err.message}`);
		}
	}

	async set(key, value, vault) {
		const getValue = await this.get(key, vault);
		if (getValue && Buffer.compare(getValue, value) === 0) {
			return;
		}
		try {
			const itemStr = JSON.stringify(Buffer.from(value).toString('base64'));
			if (getValue) {
				await this.secretsmanager
					.updateSecret({
						SecretId: `${vault}/${key}`,
						SecretString: itemStr,
					})
					.promise();
				return;
			}
			await this.secretsmanager
				.createSecret({
					Name: `${vault}/${key}`,
					SecretString: itemStr,
				})
				.promise();
			return;
		}
		catch (err) {
			throw new Error(`error from AWS SDK - ${err.message}`);
		}
	}
}

module.exports = AWSSecretsManagerStore;
