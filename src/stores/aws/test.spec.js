/* eslint-disable no-unused-vars */
'use strict';

const sinon = require('sinon');
const AWSMock = require('aws-sdk-mock');
const AWS = require('aws-sdk');
AWSMock.setSDKInstance(AWS);

const AwsSecretsManagerStore = require('./index');

describe('AwsSecretsManagerStore', function () {
	it('should be a class', function () {
		const secretsstore = new AwsSecretsManagerStore();
		expect(secretsstore).to.be.a.instanceof(AwsSecretsManagerStore);
	});

	it('should provide "get" method', function () {
		const secretsstore = new AwsSecretsManagerStore();
		expect(secretsstore)
			.to.have.property('get')
			.that.is.a('function');
	});

	describe('get method', function () {
		const secret = {
			vault: 'vault',
			key: 'key',
			value: 'value',
			encodedValue: 'dmFsdWU=',
		};

		afterEach(function () {
			AWSMock.restore('SecretsManager');
		});
		describe('passed valid key', function () {
			it('should return value', async function () {
				AWSMock.mock(
					'SecretsManager',
					'getSecretValue',
					new Promise((resolve, reject) => resolve({SecretString: secret.encodedValue}))
				);
				const secretsstore = new AwsSecretsManagerStore();
				const base64Data = await secretsstore.get(secret.key, secret.vault);
				expect(base64Data.toString()).to.equal(secret.value);
			});
		});
		describe('passed invalid key', function () {
			it('should return undefined', async function () {
				AWSMock.mock(
					'SecretsManager',
					'getSecretValue',
					new Promise((resolve, reject) => reject(new Error('can’t find')))
				);
				const secretsstore = new AwsSecretsManagerStore();
				const base64Data = await secretsstore.get(secret.key, secret.vault);
				expect(base64Data).to.be.undefined;
			});
		});
		describe('passed key of "kube HASH_SECRET" which is not base64 encoded on ', function () {
			it('should return value', async function () {
				AWSMock.mock(
					'SecretsManager',
					'getSecretValue',
					new Promise((resolve, reject) => resolve({SecretString: secret.value}))
				);
				const secretsstore = new AwsSecretsManagerStore({hashSecretKeyName: secret.key});
				const data = await secretsstore.get(secret.key, secret.vault);
				expect(data).to.eql(secret.value);
			});
		});
	});

	it('should provide "set" method', function () {
		const secretsstore = new AwsSecretsManagerStore();
		expect(secretsstore)
			.to.have.property('set')
			.that.is.a('function');
	});

	describe('set method', function () {
		const secret = {
			vault: 'vault',
			key: 'key',
			value: 'value',
			encodedValue: 'dmFsdWU=',
		};
		afterEach(function () {
			AWSMock.restore('SecretsManager');
		});

		describe('passed a new key', function () {
			it('should set the value in Secrets Manager', async function () {
				AWSMock.mock(
					'SecretsManager',
					'getSecretValue',
					new Promise((resolve, reject) => reject(new Error('can’t find')))
				);
				const spy = sinon.spy();
				AWSMock.mock(
					'SecretsManager',
					'createSecret',
					(params) => new Promise((resolve, reject) => {
						resolve(spy(params));
					})
				);
				const secretsstore = new AwsSecretsManagerStore();
				await secretsstore.set(secret.key, secret.value, secret.vault);
				expect(
					spy.calledWith({
						Name: `${secret.vault}/${secret.key}`,
						SecretString: `"${secret.encodedValue}"`,
					})
				).to.be.true;
			});
		});
		describe('passed new value for an existing key', function () {
			it('should set the value in Secrets Manager', async function () {
				AWSMock.mock(
					'SecretsManager',
					'getSecretValue',
					new Promise((resolve, reject) => resolve({
						SecretString: Buffer.from('example').toString('base64'),
					})
					)
				);
				const spy = sinon.spy();
				AWSMock.mock(
					'SecretsManager',
					'updateSecret',
					(params) => new Promise((resolve, reject) => {
						resolve(spy(params));
					})
				);
				const secretsstore = new AwsSecretsManagerStore();
				await secretsstore.set(secret.key, Buffer.from(secret.value), secret.vault);
				expect(
					spy.calledWith({
						SecretId: `${secret.vault}/${secret.key}`,
						SecretString: `"${secret.encodedValue}"`,
					})
				).to.be.true;
			});
		});
		describe('passed the same value for an existing key', function () {
			it('should set the value in 1Password', async function () {
				AWSMock.mock(
					'SecretsManager',
					'getSecretValue',
					new Promise((resolve, reject) => resolve({
						SecretString: Buffer.from(secret.value).toString('base64'),
					})
					)
				);
				const spy = sinon.spy();
				AWSMock.mock(
					'SecretsManager',
					'createSecret',
					(params) => new Promise((resolve, reject) => {
						resolve(spy(params));
					})
				);
				const secretsstore = new AwsSecretsManagerStore();
				await secretsstore.set(secret.key, Buffer.from(secret.value), secret.vault);
				expect(spy.notCalled).to.be.ok;
			});
		});
	});
});
