'use strict';

const {secretKeyPrefix} = require('../../../config/defaults');

module.exports = (hash) => {
	return `${secretKeyPrefix}${hash}`;
};
