'use strict';

module.exports = async () => {
	const chunkList = [];
	return new Promise((resolve) => {
		process.stdin.on('data', (chunk) => {
			chunkList.push(chunk);
		});

		process.stdin.on('end', () => {
			resolve(Buffer.concat(chunkList));
		});
	});
};
