/* eslint-disable no-sync */
/* eslint-disable security/detect-non-literal-fs-filename */
'use strict';

const fs = require('fs');
const yaml = require('js-yaml');
const set = require('lodash.set');
const defaults = require('../../../config/defaults');

module.exports = async (yamlFile, path, value) => {
	const valuesObj = yaml.safeLoad(fs.readFileSync(yamlFile, 'utf8'));
	set(valuesObj, path, value);
	const fileContent = yaml.safeDump(valuesObj, defaults.yamlStyle);
	fs.writeFileSync(yamlFile, fileContent, 'utf8');
};
