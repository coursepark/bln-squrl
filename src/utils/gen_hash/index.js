'use strict';

const crypto = require('crypto');

module.exports = (value, hashSecret) => {
	return crypto
		.createHash('sha256')
		.update(Buffer.from(hashSecret))
		.update(value)
		.digest('base64');
};
