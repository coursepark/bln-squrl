'use strict';

const get = require('lodash.get');

module.exports = (objPathList, contentObj, deep) => {
	const objPList = objPathList.concat();
	if (objPList.length === 0) {
		objPList.push('');
	}
	for (let i = 0; i < objPList.length; i++) {
		const path = objPList[i];
		const leaf = path ? get(contentObj, path) : contentObj;
		if (typeof leaf === 'string') {
			continue;
		}
		objPList.splice(i, 1);
		i--;
		if (leaf && typeof leaf === 'object') {
			for (const prop in leaf) {
				if (deep || typeof leaf[prop] === 'string') {
					objPList.push(`${path ? `${path}.` : ''}${prop}`);
				}
			}
		}
	}
	return objPList.filter((x, i, arr) => i === arr.indexOf(x)).sort();
};
