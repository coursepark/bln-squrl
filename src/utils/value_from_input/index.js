'use strict';

const validateEncoding = require('../validate_encoding');
const readStdin = require('../read_stdin');

module.exports = async (argValue, options) => {
	if (argValue === undefined && !options.stdin) {
		throw new Error('[value] argument or --stdin option must be used');
	}
	const encoding = validateEncoding(options.encoding, 'utf8');
	const encodedValue = (options.stdin && await readStdin()) || (argValue && Buffer.from(argValue));
	try {
		return Buffer.from(encodedValue.toString(), encoding);
	}
	catch (err) {
		throw new Error(`decoding ${options.stdin ? 'stdin' : '[value]'} failed with encoding "${encoding}"`);
	}
};
