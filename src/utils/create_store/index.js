'use strict';

const stores = require('../../stores');

module.exports = ({store, options = {}}) => {
	if (!Object.keys(stores).includes(store)) {
		throw new Error(`Unsupported store provided. Available stores: ${Object.keys(stores)}`);
	}
	const Store = stores[store];
	return new Store(options);
};
