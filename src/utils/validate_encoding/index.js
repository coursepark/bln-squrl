'use strict';

const {acceptedEncodingSet} = require('../../../config/defaults');

module.exports = (encoding, defaultEncoding) => {
	if (!encoding) {
		return defaultEncoding;
	}
	if (!acceptedEncodingSet.includes(encoding)) {
		throw new Error(`encoding "${encoding}" is not recognized`);
	}
	return encoding;
};
