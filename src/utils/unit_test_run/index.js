'use strict';

module.exports = class UnitTestRun {
	constructor(fn, ClassDefinition) {
		if (ClassDefinition && typeof fn !== 'string') {
			throw new Error('UnitTestRun: constructor - fn argument must be string if ClassDefinition argument used');
		}
		if (!ClassDefinition && typeof fn !== 'function') {
			throw new Error('UnitTestRun: constructor - fn argument must be a function');
		}
		this.ClassDefinition = ClassDefinition;
		this.fn = fn;
	}

	construct(...args) {
		this.fnObject = new this.ClassDefinition(...args);
		return this;
	}

	async run(...args) {
		this.returns = undefined;
		this.error = undefined;
		if (this.ClassDefinition && !this.fnObject) {
			throw new Error('UnitTestRun: must use .construct before .run if ClassDefintion defined in constructor');
		}
		try {
			this.returns = await (this.fnObject ? this.fnObject[this.fn](...args) : this.fn(...args));
		}
		catch (err) {
			this.error = err;
		}
	}
};
