'use strict';

const stores = require('../../stores');
const parseReferenceKey = require('../parse_reference_key');

const revealFromReferenceKey = async (options = {}) => {
	const {encoding, referenceKey, Squrl} = options;

	const {key: secretKey, vault} = parseReferenceKey(referenceKey);
	if (!vault || !secretKey) {
		return {};
	}

	const storeList = options.store ? [options.store] : Object.keys(stores);

	for (const store in storeList) {
		const squrl = new Squrl({...options, store: storeList[store], vault, encoding});
		const value = await squrl.retrieve({key: secretKey, vault});
		if (value) {
			return {value, secretKey, vault};
		}
	}
	return {secretKey, vault};
};

module.exports = revealFromReferenceKey;
