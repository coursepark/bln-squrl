'use strict';

module.exports = (key, vault) => {
	return `${vault}:${key}`;
};
