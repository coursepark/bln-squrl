'use strict';

const defaults = require('../../../config/defaults');

module.exports = (refKey) => {
	if (!defaults.secretKeyRegex.test(refKey)) {
		return {};
	}
	const pl = refKey.split(':', 2);
	if (!pl[1] || !pl[1].startsWith(defaults.secretKeyPrefix)) {
		return {};
	}
	return {
		vault: pl[0],
		key: pl[1],
	};
};
