/* eslint-disable security/detect-non-literal-fs-filename */
'use strict';

const fs = require('fs');
const yaml = require('js-yaml');
const get = require('lodash.get');
const parseReferenceKey = require('../parse_reference_key');

module.exports = async (yamlFile, path) => {
	const valuesObj = yaml.safeLoad(fs.readFileSync(yamlFile, 'utf8')); // eslint-disable-line no-sync
	const referenceKey = get(valuesObj, path);
	if (typeof referenceKey !== 'string') {
		return undefined;
	}
	const {key: secretKey, vault} = parseReferenceKey(referenceKey);
	if (!vault || !secretKey) {
		return undefined;
	}
	return {secretKey, vault};
};
