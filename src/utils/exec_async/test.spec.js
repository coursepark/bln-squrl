/* eslint-disable promise/prefer-await-to-callbacks, no-sync */
/* eslint-disable no-unused-vars */

'use strict';

const childProcess = require('child_process'); // eslint-disable-line security/detect-child-process
const execAsync = require('./index');
const sinon = require('sinon');

describe('execAsync', function () {
	it('should promisify a child_process exec call and get stdout', async function () {
		sinon.stub(childProcess, 'exec').callsArgWith(2, undefined, {stdout: '<stdout>'});

		const stdout = await execAsync.execAsync('<command> <arg>', {a: '<option_a>'});

		expect(stdout).to.equal('<stdout>');
		expect(childProcess.exec.firstCall.args[0]).to.equal('<command> <arg>');
		expect(childProcess.exec.firstCall.args[1]).to.deep.equal({a: '<option_a>'});
		childProcess.exec.restore();
	});

	it('should promisify a child_process exec call and get stderr as promise reject', async function () {
		sinon.stub(childProcess, 'exec').callsArgWith(2, '<stderr>', {stdout: '<stdout>'});

		let stderr;
		try {
			await execAsync.execAsync('<command> <arg>', {a: '<option_a>'});
		}
		catch (e) {
			stderr = e;
		}

		expect(stderr).to.equal('<stderr>');
		expect(childProcess.exec.firstCall.args[0]).to.equal('<command> <arg>');
		expect(childProcess.exec.firstCall.args[1]).to.deep.equal({a: '<option_a>'});
		childProcess.exec.restore();
	});
});
