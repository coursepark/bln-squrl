/* eslint-disable no-constant-condition */
'use strict';

const {promisify} = require('util');
const childProcess = require('child_process'); // eslint-disable-line security/detect-child-process

const RETRY_ATTEMPTS_DEFAULT = 5;

const execAsync = {
	async execAsync(command, options) {
		// eslint-disable-next-line security/detect-child-process
		return (await promisify(childProcess.exec)(command, options)).stdout;
	},
	// eslint-disable-next-line max-len
	async execAsyncRetry(command, options, onAttemptFailAsyncFn) {
		const defaultOptions = {
			maxAttempts: RETRY_ATTEMPTS_DEFAULT,
		};
		const _options = Object.assign({}, defaultOptions, options);
		let attempt = 0;
		while (true) {
			// eslint-disable-line no-constant-condition
			try {
				return await execAsync.execAsync(command, _options); // eslint-disable-line no-await-in-loop
			}
			catch (err) {
				if (attempt === _options.maxAttempts) {
					throw err;
				}
				if (onAttemptFailAsyncFn) {
					await onAttemptFailAsyncFn(err); // eslint-disable-line no-await-in-loop
				}
			}
			attempt++;
		}
	},
};
module.exports = execAsync;
