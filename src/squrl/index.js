'use strict';

const defaults = require('../../config/defaults');
const {genKey, genHash, parseReferenceKey, genReferenceKey, createStore} = require('../utils');

class Squrl {
	constructor(options = {}) {
		this.storeType = options.store || 'aws';
		this.vault = options.vault || 'squrl';

		this.secretStore = createStore({store: this.storeType, options});
		this.hashSecretKeyName = options.hashSecretKeyName || defaults.hashSecretKeyName;
		this.encoding = options.encoding || defaults.encoding;
	}

	async create({value}) {
		if (!value) {
			throw new Error('No secret value provided');
		}

		const hashSecret = await this.secretStore.get(this.hashSecretKeyName, this.vault);
		const key = genKey(genHash(value, hashSecret));
		await this.secretStore.set(key, Buffer.from(value), this.vault);
		return genReferenceKey(key, this.vault);
	}

	async reveal({referenceKey}) {
		if (!referenceKey) {
			throw new Error('No reference key provided');
		}
		const {key: secretKey, vault} = parseReferenceKey(referenceKey);

		const value = await this.secretStore.get(secretKey, vault);
		if (value === undefined) {
			return undefined;
		}
		return value.toString(this.encoding);
	}

	async retrieve({vault, key}) {
		if (!key) {
			throw new Error('No key specified for retrieval');
		}
		const fromVault = vault || this.vault;
		const value = await this.secretStore.get(key, fromVault);
		if (value === undefined) {
			return undefined;
		}
		return value.toString(this.encoding);
	}

	async store({vault, key, value}) {
		if (!vault) {
			throw new Error('Vault not specified');
		}

		if (!key) {
			throw new Error('No secret key provided');
		}

		if (!value) {
			throw new Error('No secret value provided');
		}

		await this.secretStore.set(key, value, vault);
		return {
			store: this.storeType,
			key,
			value,
			vault,
		};
	}
}

module.exports = Squrl;
