'use strict';

const Squrl = require('./index');
// const sinon = require('sinon');

describe('Squrl', function () {
	it('should be a class', function () {
		const squrl = new Squrl();
		expect(squrl).to.be.an.instanceof(Squrl);
	});

	it('should have a default store', function () {
		const squrl = new Squrl();
		expect(squrl.storeType).to.exist;
	});
	it('should have a default vault', function () {
		const squrl = new Squrl();
		expect(squrl.vault).to.exist;
	});

	it('should throw an error if store is not supported', function () {
		const store = 'unsupported';
		expect(() => new Squrl({store})).to.throw();
	});
});
