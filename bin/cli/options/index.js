/* eslint-disable no-process-env */
'use strict';

const stores = require('../../../src/stores');
const defaults = require('../../../config/defaults');

module.exports = {
	opOption: (commandObj) => {
		commandObj
			.option('--opShorthand <shorthand>', '1Password shorthand, use with --opAccount and --opSecretKey')
			.option(
				'--opAccount <account>',
				'1Password account, "<account>.1password.com", use with --opEmail and --opSecretKey, or --opSession'
			)
			.option('--opEmail <email>', '1Password email, use with --opAccount and --opSecretKey')
			.option('--opSecretKey <secret-key>', '1Password secret key, use with --opAccount and --opEmail')
			.option('--opPassword <password>', '1Password password, not recommended - often captured in logs')
			.option(
				'--opSession <session>',
				'1Password session, can be used instead of --opEmail, --opSecretKey, use with --opAccount'
			);
	},

	awsOption: (commandObj) => {
		commandObj.option('--region <region>', 'Specify AWS Secrets Manager region');
	},

	mapOption: (options) => {
		return {
			shorthand: options.opShorthand,
			account: options.opAccount,
			email: options.opEmail,
			secretKey: options.opSecretKey,
			password: options.opPassword,
			session: options.opSession,
			region: options.region,
		};
	},

	fileOption: (commandObj) => {
		commandObj
			.option('-i, --input-file <file>', 'input file, takes precendence over --file argument')
			// eslint-disable-next-line max-len
			.option('-o, --output-file <file>', 'output file, overwrites existing, takes precendence over --file argument')
			.option('-f, --file <file>', 'file to replace');
	},

	storeOption: (commandObj) => {
		commandObj.option(
			'-s, --store <store>',
			`which secret store to use, default is 'aws' ${JSON.stringify(Object.keys(stores))}`,
			'op'
		);
	},

	storeOptionNoDefault: (commandObj) => {
		commandObj.option(
			'-s, --store <store>',
			'which secret store to use, optional',
		);
	},

	vaultOption: (commandObj) => {
		// eslint-disable-next-line max-len
		commandObj.option('-v, --vault <vault>', `The 1Password vault for the values. Defaults to "${defaults.vault}"`);
	},

	vaultFromOptions: (options) => {
		if (options && options.vault) {
			return options.vault;
		}
		throw new Error('Vault not specified. please specifiy vault on your secret store.');
	},

	inputOption: (commandObj) => {
		commandObj.option('--stdin', 'use stdin instead of [value] argument');
		commandObj.option(
			'-e, --encoding <encoding>',
			`encoding of the value or stdin, default is utf8 ${JSON.stringify(defaults.acceptedEncodingSet)}`
		);
	},

	outputOption: (commandObj) => {
		commandObj.option(
			'-e, --encoding <encoding>',
			`encoding of the stdout, default is utf8 ${JSON.stringify(defaults.acceptedEncodingSet)}`
		);
	},

	debugOption: (commandObj) => {
		commandObj.option('--debug', 'detailed error messages for debugging');
	},
	includeEnvOptions: (options) => ({
		...options,
		vault: options.vault || process.env.SQURL_DEFAULT_VAULT,
		store: options.store || process.env.SQURL_DEFAULT_STORE,
		hashSecretKeyName: process.env.SQURL_HASH_SECRET_KEY_NAME || defaults.hashSecretKeyName,
	}),
};
