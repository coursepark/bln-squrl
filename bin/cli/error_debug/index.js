'use strict';

const {includeEnvOptions} = require('../options');

module.exports = (fn) => {
	function fail(err, debug) {
		if (debug) {
			console.error(err); // eslint-disable-line no-console
		}
		else {
			process.stderr.write(`${err.message}\n`);
		}
		process.exitCode = 1;
	}
	return async function (...args) {
		try {
			const argsWithEnvVars = [...args, includeEnvOptions(args[args.length - 1])];
			const p = fn(...argsWithEnvVars);
			await Promise.resolve(p);
		}
		catch (err) {
			fail(err, args[args.length - 1].debug);
		}
	};
};
