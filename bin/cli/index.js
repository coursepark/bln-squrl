#!/usr/bin/env node
/* eslint-disable security/detect-non-literal-fs-filename */

'use strict';

process.title = 'squrl'; // vanity mostly

const crypto = require('crypto');
const fs = require('fs');
const get = require('lodash.get'); // get value by object navigation
const program = require('commander'); // cli
const set = require('lodash.set'); // set value by object navigation
const yaml = require('js-yaml'); // convert to and from js objects to yaml

const SessionProvider = require('../../src/stores/op/session');

const Squrl = require('../../src/squrl');
const utils = require('../../src/utils');
const defaults = require('../../config/defaults');

const errorDebug = require('./error_debug');

const {
	opOption,
	awsOption,
	mapOption,
	fileOption,
	storeOption,
	storeOptionNoDefault,
	vaultOption,
	vaultFromOptions,
	inputOption,
	outputOption,
	debugOption,
} = require('./options');

// need to be able to bulk add options, commander doesn't support that out-of-the-box, hack around it
// wrap the program.command with version that adds options method
// options method takes functions as param which are called when executed, passing each the commandObj
program.wrappedCommand = program.command;
program.command = function (...commandArgs) {
	const commandObj = this.wrappedCommand(...commandArgs);
	commandObj.optionX = function (...fnList) {
		for (const fn of fnList) {
			fn(this);
		}
		return this;
	};
	return commandObj;
};

// return an array of object paths with any referenced objects expanded to their immediate keys
// paths that reference non strings are filtered out

program
	.command('encode <obj-path...>')
	.description('encode values at specified obj-paths of provided yaml')
	.optionX(fileOption)
	.option(
		'-e, --encoding <encoding>',
		`encoding of the input values, default is base64 ${JSON.stringify(defaults.acceptedEncodingSet)}`
	)
	.optionX(vaultOption, opOption, awsOption, debugOption, storeOption)
	.action(
		errorDebug(async (objPathList, options) => {
			Object.assign(options, mapOption(options));
			const vault = vaultFromOptions(options);
			const input
				= options.inputFile || options.file
					? fs.readFileSync(options.inputFile || options.file, 'utf8') // eslint-disable-line no-sync
					: await utils.readStdin();
			const contentObj = yaml.safeLoad(input);
			const encoding = utils.validateEncoding(options.encoding, 'base64');
			const objPList = utils.objPathNormalize(objPathList, contentObj);

			const squrl = new Squrl({...options, store: options.store, vault});

			for (const objPath of objPList) {
				const rawValue = get(contentObj, objPath);
				const {key: rawKey, vault: rawVault} = utils.parseReferenceKey(rawValue);
				if (rawKey || rawVault) {
					throw new Error(`${objPath} is already encoded`);
				}
				const value = Buffer.from(rawValue, encoding);
				const secret = await squrl.create({value}); // eslint-disable-line no-await-in-loop
				set(contentObj, objPath, secret);
			}

			const output = yaml.safeDump(contentObj, defaults.yamlStyle);
			if (options.outputFile || options.file) {
				fs.writeFileSync(options.outputFile || options.file, output, 'utf8'); // eslint-disable-line no-sync
			}
			else {
				process.stdout.write(output);
			}
		})
	);

program
	.command('decode [obj-path...]')
	.description('decode values at specified obj-paths of provided yaml')
	.optionX(fileOption)
	.option('-d, --deep', 'include nested values')
	.option(
		'-e, --encoding <encoding>',
		`encoding of the output values, default is base64 ${JSON.stringify(defaults.acceptedEncodingSet)}`
	)
	.optionX(opOption, awsOption, debugOption, storeOptionNoDefault)
	.action(
		errorDebug(async (objPathList, options) => {
			Object.assign(options, mapOption(options));
			const encoding = utils.validateEncoding(options.encoding, 'base64');
			const input
				= options.inputFile || options.file
					? fs.readFileSync(options.inputFile || options.file, 'utf8') // eslint-disable-line no-sync
					: await utils.readStdin();
			const contentObj = yaml.safeLoad(input);
			const objPList = utils.objPathNormalize(objPathList, contentObj, options.deep);

			for (const objPath of objPList) {
				const referenceKey = get(contentObj, objPath);

				// eslint-disable-next-line no-await-in-loop
				const {value,
					secretKey,
					vault} = await utils.revealFromReferenceKey({...options, encoding, referenceKey, Squrl});

				if (!vault || !secretKey) {
					continue;
				}

				if (value === undefined) {
					throw new Error(`"${secretKey}" not found in "${vault}"`);
				}
				set(contentObj, objPath, value.toString(encoding));
			}

			const output = yaml.safeDump(contentObj, defaults.yamlStyle);
			if (options.outputFile || options.file) {
				fs.writeFileSync(options.outputFile || options.file, output, 'utf8'); // eslint-disable-line no-sync
			}
			else {
				process.stdout.write(output);
			}
		})
	);

program
	.command('hash-assert <hash> [value]')
	.description('exit with code 0 if hash argument matches the hash generated from the value argument or stdin provided') // eslint-disable-line max-len
	.option('-d, --descriptive')
	.optionX(inputOption, vaultOption, opOption, awsOption, debugOption, storeOption)
	.action(
		errorDebug(async (hash, argValue, options) => {
			Object.assign(options, mapOption(options));
			const value = await utils.valueFromInput(argValue, options);
			const vault = vaultFromOptions(options);
			const squrl = new Squrl({...options, store: options.store, vault});
			const hashSecret = await squrl.retrieve(defaults.hashSecretKeyName);
			const hashFromValue = utils.genHash(value, hashSecret);
			const match = crypto.timingSafeEqual(Buffer.from(hash), Buffer.from(hashFromValue));
			if (options.descriptive) {
				process.stdout.write(
					`hash:       ${hash}\nvalue hash: ${hashFromValue}\n${match ? 'matches' : 'does not match'}\n`
				);
			}
			if (!match) {
				process.exitCode = 1;
			}
		})
	);

program
	.command('create [value]')
	.description('create a secret with the value provided and output the reference-key')
	.optionX(inputOption, vaultOption, opOption, debugOption, awsOption, storeOption)
	.action(
		errorDebug(async (argValue, options) => {
			Object.assign(options, mapOption(options));
			const {hashSecretKeyName} = defaults;
			const squrl = new Squrl({
				...options,
				hashSecretKeyName,
				vault: vaultFromOptions(options),
			});
			const value = await utils.valueFromInput(argValue, options);
			const referenceKey = await squrl.create({value});
			process.stdout.write(`${referenceKey}\n`);
		})
	);

program
	.command('reveal <reference-key>')
	.description('get the value of a secret in the specified <reference-key> argument')
	.option('-p, --precise', 'prevent output of additional trailing newline character')
	.optionX(outputOption, opOption, awsOption, debugOption, storeOptionNoDefault)
	.action(
		errorDebug(async (referenceKey, options) => {
			Object.assign(options, mapOption(options));
			const encoding = utils.validateEncoding(options.encoding, 'utf8');
			const {value,
				secretKey,
				vault} = await utils.revealFromReferenceKey({...options, encoding, referenceKey, Squrl});
			if (value === undefined) {
				throw new Error(`"${secretKey}" not found in "${vault}"`);
			}

			process.stdout.write(value);
			if (!options.precise) {
				process.stdout.write('\n');
			}
		})
	);

program
	.command('hash-gen [value]')
	.description('output hash of value argument or stdin provided')
	.optionX(inputOption, vaultOption, opOption, debugOption, storeOption)
	.action(
		errorDebug(async (argValue, options) => {
			Object.assign(options, mapOption(options));
			const value = await utils.valueFromInput(argValue, options);
			const vault = vaultFromOptions(options);
			const squrl = new Squrl({...options, store: options.store, vault});
			const hashSecretKey = squrl.retrieve({key: defaults.hashSecretKeyName, vault});
			process.stdout.write(`${utils.genHash(value, hashSecretKey)}\n`);
		})
	);

program
	.command('set <yaml-file> <obj-path> [value]')
	.description('set a secret in a specified file at a particular obj-path from a value')
	.optionX(inputOption, vaultOption, opOption, awsOption, debugOption, storeOption)
	.action(
		errorDebug(async (yamlFile, objPath, argValue, options) => {
			Object.assign(options, mapOption(options));
			const value = await utils.valueFromInput(argValue, options);
			const vault = utils.vaultFromOptions(options);
			const squrl = new Squrl({...options, store: options.store, vault});
			const secret = squrl.create({value});
			await utils.valueSet(yamlFile, objPath, secret);
		})
	);

program
	.command('get <yaml-file> <obj-path>')
	.description('get the value of a secret in a specified file at a particular obj-path')
	.optionX(outputOption, opOption, awsOption, debugOption, storeOption)
	.action(
		errorDebug(async (yamlFile, objPath, options) => {
			Object.assign(options, mapOption(options));
			const encoding = utils.validateEncoding(options.encoding, 'utf8');
			const {secretKey, vault} = await utils.valueGet(yamlFile, objPath, options);
			const squrl = new Squrl({...options, store: options.store, vault});
			const value = squrl.retrieve(secretKey, vault);
			if (value === undefined) {
				process.exitCode = 1;
				return;
			}
			process.stdout.write(`${value.toString(encoding)}\n`);
		})
	);

program
	.command('get-session')
	.description('get a 1Password session, useful for debugging')
	.option('-i, --invalidate')
	.optionX(opOption, debugOption)
	.action(
		errorDebug(async (options) => {
			Object.assign(options, mapOption(options));
			const sessionProvider = new SessionProvider(mapOption(options));
			const {session} = await sessionProvider.get(Boolean(options.invalidate));
			process.stdout.write(`${session}\n`);
		})
	);

program.version('1.0.0');
program.description(
	''
		+ 'Utility for secret management with bln-cluster-kube and 1Password. '
		+ 'Notes: passwords are not stored, 1Password '
		+ 'sessions have half hour expiry.'
);

program.parse(process.argv);
if (!process.argv.slice(2).length) {
	program.outputHelp();
}
