'use strict';

module.exports = {
	extends: [
		'bluedrop',
		'bluedrop/config/node',
		'bluedrop/config/ecmascript-8',
		'bluedrop/config/chai',
		'bluedrop/config/mocha',
	],
	parserOptions: {
		ecmaVersion: 2018,
	},
};
